package com.app.product;

import java.io.IOException;
import java.util.List;

class Result {

    /*
     * Complete the 'countingValleys' function below.
     *
     * The function is expected to return an INTEGER. The function accepts
     * following parameters: 1. INTEGER steps 2. STRING path
     */
    // >
    public static long repeatedString(String s, long n) {

        long slength= s.length();
        
        
        long result=0;

        if (s.length() == 1) {
            if (s.equals("a"))
                return n;
            else
                return 0;
        }

        long strCap = n/slength;
        int acount= (int) s.chars().filter(ch -> ch == 'a').count();
        result=(long) (strCap*acount);
        long current= strCap*slength;
        long re = n-current;
        
        String sub= s.substring(0, (int)re);
        long asubcount= (long) sub.chars().filter(ch -> ch == 'a').count();
        return result=result+asubcount;
        
        
//        while (n >= count) {
//            for (Character c : s.toCharArray()) {
//                str.append(c);
//
//            }
//
//            count = (int) (count + s.length());
//            if (count > n) {
//
//                int re = (int) (count - n);
//                str.replace((int) n, count, "");
//                // str.append(getString(s, count, n));
//
//            }
//        }
//
//        // Write your code here
//        System.out.println(str.toString().length());
//
//        System.out.println(str.toString().length());
//        return str.toString().chars().filter(ch -> ch == 'a').count();
//
//    }
    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        int result = (int) Result.repeatedString("kmretasscityylpdhuwjirnqimlkcgxubxmsxpypgzxtenweirknjtasxtvxemtwxuarabssvqdnktqadhyktagjxoanknhgilnm", 736778906400l);

//        int result = (int) Result.repeatedString("aba", 10);
        System.out.println(result);
    }
}
